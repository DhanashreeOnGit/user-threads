# user-threads


                                                           MULTITHREADING LIBRARY

## **Table Of Contents**
- [Description](https://gitlab.com/DhanashreeOnGit/user-threads/-/blob/main/README.md#description)
- [Multithreading Models](https://gitlab.com/DhanashreeOnGit/user-threads/-/blob/main/README.md#multithreading-models)
- [Reference](https://gitlab.com/DhanashreeOnGit/user-threads#reference)

----
## **Description**

Implementing a user level multithreading library.
The following two multithreading models are implemented in this repository:
- **Many-One Model**
- **Many-Many Model**
----

## **Multithreading Models**

**1. Many to one multithreading model:**

The "many to one" threading model is a type of threading model in which multiple user-level threads are mapped to a single kernel-level thread. In this model, all user-level threads are managed by a single thread in the operating system's kernel. 
<br>
In the many-to-one threading model, all thread management is handled by the user-level thread library rather than the operating system. This means that thread creation, scheduling, and synchronization are performed at the user level, and context switching between threads is not handled by the operating system's scheduler. Instead, the user-level thread library manages the scheduling of threads.


![](https://static.javatpoint.com/operating-system/images/multithreading-models-in-operating-system3.png "Many-To-One Model")


In the above figure, the many to one model associates all user-level threads to single kernel-level threads.

**2. Many to Many Model multithreading model**

The "many-to-many" threading model is a type of threading model in which multiple user-level threads are mapped to an equal or lesser number of kernel-level threads. In this model, the user-level thread library manages multiple threads and maps them to the available kernel-level threads.
<br>
In the many-to-many threading model, the thread management is handled by both the user-level thread library and the operating system. The user-level thread library is responsible for creating and managing user-level threads, while the operating system is responsible for managing kernel-level threads. The library schedules user-level threads onto kernel-level threads based on the availability of the latter.
<br>
This model offers several advantages over the many-to-one threading model. First, it enables multiple user-level threads to run concurrently on multiple processor cores, allowing for parallel execution of multiple threads. Second, it avoids the issue of thread blocking, as the operating system can allocate multiple kernel-level threads to the user-level threads, enabling them to execute simultaneously.

![](https://static.javatpoint.com/operating-system/images/multithreading-models-in-operating-system5.png "Many-To-Many Model")

Many to many versions of the multithreading model associate several user-level threads to the same or much less variety of kernel-level threads in the above figure.

----

## **Reference**
- [Image Reference](https://www.javatpoint.com/multithreading-models-in-operating-system)

- [Basic Information](https://www.cs.uic.edu/~jbell/CourseNotes/OperatingSystems/4_Threads.html)