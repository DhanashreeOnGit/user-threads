#include <stdio.h>
#include <stdlib.h>
#include "thread.h"

void *thread1(void *arg)
{
	int j = *((int *)arg), k = 1, l;
	for(int i=1; i++ <= j; )
	{
		for(l = 0; l < 10000; l++);
		printf("I am in thread 1: k = %d\n", k++);
	}
	return NULL;
}

void *thread2(void *arg)
{
	int j = *((int *)arg), k = 1, l;
	for(int i=1; i++ <= j; )
	{
		for(l = 0; l < 10000; l++);
		printf("I am in thread 2: k = %d\n", k++);
	}
	return NULL;
}

int main(int argc, char *argv[])
{
	thread_t th1, th2;
	int r;
	if(argc < 2)
	{
		printf("%s Total Repetitions\n", argv[0]);
		exit(0);
	}
	r = atoi(argv[1]);
	thread_init();
	printf("creating thread 1\n");
	thread_create(&th1, thread1, &r);
	printf("creating thread 2\n");
	thread_create(&th2, thread2, &r);
	printf("calling join on thread1\n");
	thread_join(th1, NULL);
	printf("calling join on thread2\n");
	thread_join(th2, NULL);
	return 0;
}
