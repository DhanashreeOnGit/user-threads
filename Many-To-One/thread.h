#ifndef THREAD_H

#define THREAD_H

#define RUNNING 0x0
#define NOT_STARTED 0x1
#define TERMINATED 0x2
#define JOIN_CALLED 0x3
#define COLLECTED 0x4

#define STACKSIZE (1024 * 1024)

typedef unsigned long int thread_t;
typedef volatile unsigned short int thread_lock_t;

typedef struct sleeplock {
  	int locked;
	spinlock lk;
	thread_id t_id;
} sleeplock;

typedef struct spinlock {
	int locked;       
	thread_id t_id;
} spinlock;

struct node_pending_signal {	
	int signal;
	struct node_pending_signal *next;
};

typedef struct queue_pending_signals {
	struct node_pending_signal *head;
	struct node_pending_signal *tail;
} queue_pending_signals;

struct thread_struct {
	int state;
	void *(*start_routine)(void *);
	void *args;
	void *returnvalue;
	signal_handler_t signalhandlers[32];
	ucontext_t thread_context;
	queue_pending_signals pending_signals;
};

struct active_thread_node {
	thread_t thread;
	ucontext_t *uc;
	struct active_thread_node *next;
};

void thread_init();
int thread_create(thread_t *thread, void *(*start_routine)(void *), void *args);
int thread_join(thread_t thread, void **returnvalue);
int thread_kill(thread_t thread, int signal);
void thread_exit(void *returnvalue);

int threadlock_init(thread_lock_t *lock);
int thread_lock(thread_lock_t *lock);
int thread_unlock(thread_lock_t *lock);

void init_mutex_thread_lock(struct sleeplock *lock)
void thread_mutex_lock(struct sleeplock *lock)
void thread_mutex_unlock(struct sleeplock *lock)

#endif
