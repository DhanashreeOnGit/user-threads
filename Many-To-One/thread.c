#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <ucontext.h>
#include <errno.h>
#include <sys/ucontext.h>
#include "thread.h"

static struct thread_struct **all_threads[16] = {0};
static int total_threads = 0, current = 0, super_lock = 0;
static ucontext_t main_context;
static struct active_thread_node *active_node = NULL, *main_thread = NULL, *previous_node = NULL, *last_node = NULL;
static signal_handler_t default_signal_handlers[32], mainthread_signal_handlers[32], sigdf[32];

static void super_lock_lock() {
	while(__sync_lock_test_and_set(&super_lock, 1));
}

static void super_lock_unlock() {
	__sync_synchronize();
	super_lock = 0;
}

static void next_thread(int signal) {
	if(signal == SIGALRM && current > 1 && !__sync_lock_test_and_set(&super_lock, 1)) {
		previous_node = active_node;
		active_node = active_node->next;
		super_lock_unlock();
	}
}

static void add_signal(queue_pending_signals *q, int signal) {
	if(q->head) {
		q->tail->next = (struct node_pending_signal *)malloc(sizeof(struct node_pending_signal));
		q->tail->next->signal = signal;
		q->tail->next->next = NULL;
		q->tail = q->tail->next;
	}
	else {
		q->head = q->tail = (struct node_pending_signal *)malloc(sizeof(struct node_pending_signal));
		q->head->signal = signal;
		q->head->next = NULL;
	}
}

int thread_create(thread_t *thread, void *(*start_routine)(void *), void *args) {
	struct thread_struct *t; 
	struct active_thread_node *new_thread;
	if(current == 1)
		ualarm(1000, 1000);
	super_lock_lock();
	*thread = total_threads;
	t->state = RUNNING;
	new_thread = (struct active_thread_node *)malloc(sizeof(struct active_thread_node));
	new_thread->thread = *thread;
	new_thread->uc = &(t->thread_context);
	new_thread->next = main_thread->next;
	main_thread->next = new_thread;
	current++;
	if(new_thread->next == main_thread)
		last_node = new_thread;
	super_lock_unlock();
	return 0;
}

void thread_init() {
	active_node = (struct active_thread_node *)malloc(sizeof(struct active_thread_node));
	active_node->thread = 0;
	active_node->uc = &main_context;
	active_node->next = active_node;
	last_node = main_thread = active_node;
	current = 1;
	for(int i = 0; i < 32; i++) 
		sigdf[i] = def_signal_handlers[i] = main_thread_signal_handlers[i] = SIG_DFL;
}

int thread_join(thread_t thread, void **returnvalue) {
	int current, location_index, status = EINVAL;
	thread--;
	current = thread / 16;
	location_index = thread % 16;
	super_lock_lock();
	if(thread < total_threads) {
		switch(all_threads[current][location_index]->state) {
		        case NOT_STARTED:
			case COLLECTED:
			case JOIN_CALLED:
				super_lock_unlock();
				break;
			case RUNNING:
				all_threads[current][location_index]->state = JOIN_CALLED;
				super_lock_unlock();
				while(all_threads[current][location_index]->state != TERMINATED)
					next_thread(SIGALRM);
				super_lock_lock();
				all_threads[current][location_index]->state = COLLECTED;
				super_lock_unlock();
				if(returnvalue)
					*returnvalue = all_threads[current][location_index]->returnvalue;
				status = 0;
				break;
			case TERMINATED:
				super_lock_unlock();
				if(returnvalue)
					*returnvalue = all_threads[current][location_index]->returnvalue;
				status = 0;
				break;
			default:
				super_lock_unlock();
				break;
		}
	}
	return status;
}

int thread_kill(thread_t thread, int signal) {
	int current, location_index;
	thread--;
	current = thread / 16;
	location_index = thread % 16;
	super_lock_lock();
	add_signal(&(all_threads[current][location_index]->pending_signals), signal);
	super_lock_unlock();
	return 0;
}

void thread_exit(void *returnvalue) {
	int index = active_node->thread - 1;
	int current = index / 16, location_index = index % 16;
	ucontext_t *this_context;
	if(index >= 0) {
		super_lock_lock();
		this_context = active_node->uc;
		all_threads[current][location_index]->state = TERMINATED;
		all_threads[current][location_index]->returnvalue = returnvalue;
		if(active_node->next == main_thread)
			last_node = previous_node;
		previous_node->next = active_node->next;
		free(active_node);
		active_node = main_thread;
		previous_node = last_node;
		current--;
		if(current == 1)
			ualarm(0, 0);
		super_lock_unlock();
	}
}

int threadlock_init(thread_lock_t *lock) {
	*lock = 0;
	return 0;
}

int thread_lock(thread_lock_t *lock) {
	if(*lock != 0 || *lock != 1)
		return EINVAL;
	while(__sync_lock_test_and_set(lock, 1));
	return 0;
}

int thread_unlock(thread_lock_t *lock) {
	if(*lock != 1)
		return EINVAL;
	__sync_synchronize();
	*lock = 0;
	return 0;
}

void init_mutex_thread_lock(struct sleeplock *lock){
        initsleeplock(lock);
}

void thread_mutex_lock(struct sleeplock *lock){
        acquiresleep(lock);
}

void thread_mutex_unlock(struct sleeplock *lock){
        releasesleep(lock);
}
